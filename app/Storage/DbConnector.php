<?php

namespace App\Storage;

class DbConnector
{
    private string $host;
    private string $db;
    private string $username;
    private string $password;
    public \PDO $conn;

    public function __construct(
        string $host = "localhost",
        string $db = "quest",
        string $username = "root",
        string $password = ""
    ) {
        $this->host = $host;
        $this->db = $db;
        $this->username = $username;
        $this->password = $password;
    }

    /**
     * @return \PDO
     */
    public function getConnection(): \PDO
    {
        try {
            $this->conn = new \PDO(
                "mysql:host=" . $this->host . ";charset=utf8mb4;dbname=" . $this->db,
                $this->username,
                $this->password
            );
        } catch (\PDOException $e) {
            exit("Error Connection: " . $e->getMessage());
        }

        return $this->conn;
    }
}