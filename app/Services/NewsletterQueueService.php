<?php

namespace App\Services;

use App\Repositories\QueueRepository;
use App\Repositories\UserRepository;
use App\Storage\DbConnector;

class NewsletterQueueService
{
    public const LIMIT = 100;

    private QueueRepository $queueRepository;
    private UserRepository $userRepository;

    public function __construct()
    {
        $conn = (new DbConnector())->getConnection();
        $this->queueRepository = new QueueRepository($conn);
        $this->userRepository = new UserRepository($conn);
    }

    /**
     * @param int $news
     * @return bool
     */
    public function createQueue(int $news): bool
    {
        if ($this->queueRepository->checkNews($news)[0] == 0) {
            return false;
        }

        $userNews = $this->queueRepository->getUsersNewsIds($news);
        if (empty($userNews)) {
            return false;
        }

        $userNews = array_map(static function ($item) {
            return [$item['user_id'], $item['news_id']];
        }, $userNews);
        return $this->queueRepository->save($userNews);
    }

    /**
     * @param int $news
     * @return bool
     */
    public function createQueueAllUsers(int $news): bool
    {
        if ($this->queueRepository->checkNews($news)[0] == 0) {
            return false;
        }

        $users = $this->userRepository->getAllUsers();
        if (empty($users)) {
            return false;
        }

        $userNews = array_map(static function ($item) use ($news) {
            return [$item['id'], $news];
        }, $users);
        return $this->queueRepository->save($userNews);
    }

    /**
     * @param int $limit
     * @return array|false
     */
    public function getQueue(int $limit = self::LIMIT)
    {
        return $this->queueRepository->getMessages($limit);
    }

    /**
     * @param int $user
     * @param int $news
     * @return array|false
     */
    public function getMessage(int $user, int $news)
    {
        return $this->queueRepository->getUserNews($user, $news);
    }

    /**
     * @param int $id
     * @return bool
     */
    public function deleteMessage(int $id): bool
    {
        return $this->queueRepository->delete($id);
    }
}