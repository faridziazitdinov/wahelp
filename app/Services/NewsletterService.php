<?php

namespace App\Services;

class NewsletterService
{
    private NewsletterQueueService $queue;

    public function __construct()
    {
        $this->queue = new NewsletterQueueService();
    }

    /**
     * @param int $news
     * @return void
     * @throws \Exception
     */
    public function sendNewsletter(int $news)
    {
        if (!$this->queue->createQueue($news)) {
            throw new \Exception("Failed Create Queue");
        }
    }

    /**
     * @param int $news
     * @return void
     * @throws \Exception
     */
    public function sendNewsletterAllUsers(int $news)
    {
        if (!$this->queue->createQueueAllUsers($news)) {
            throw new \Exception("Failed Create Queue");
        }
    }

    /**
     * @return void
     */
    public function asyncSendNewsletter()
    {
        $messages = $this->queue->getQueue();
        $sender = new SendService();
        foreach ($messages as $message) {
            $userNews = $this->queue->getMessage($message["user_id"], $message["news_id"]);
            if ($userNews) {
                $userNews = current($userNews);
                if ($sender->send($userNews["news_name"], $userNews["content"], $userNews["name"])) {
                    $this->queue->deleteMessage($message["id"]);
                }
            }
        }
    }
}