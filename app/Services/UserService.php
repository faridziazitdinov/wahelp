<?php

namespace App\Services;

use App\Repositories\UserRepository;
use App\Storage\DbConnector;

class UserService
{
    private UserRepository $userRepository;

    public function __construct()
    {
        $conn = (new DbConnector())->getConnection();
        $this->userRepository = new UserRepository($conn);
    }

    /**
     * @param string $filePath
     * @return void
     * @throws \Exception
     */
    public function saveUsers(string $filePath)
    {
        $lines = file($filePath, FILE_SKIP_EMPTY_LINES | FILE_IGNORE_NEW_LINES);
        $users = array_map(static function ($line) {
            return explode(",", $line, 2);
        }, $lines);

        $this->userRepository->saveUsersV2($users);
    }
}