<?php

namespace App\Entity;

class Request
{
    public array $body;

    public string $method;

    public array $uri;

    public array $files;

    public function __construct(string $method, array $uri, array $body, array $files)
    {
        $this->method = $method;
        $this->uri = $uri;
        $this->body = $body;
        $this->files = $files;
    }
}