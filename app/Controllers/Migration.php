<?php

namespace App\Controllers;

use App\Entity\Request;
use App\Storage\DbConnector;

class Migration extends Controller
{
    public function __construct()
    {
        header("Access-Control-Allow-Methods: POST");
        parent::__construct();
    }

    /**
     * @param Request $request
     * @return string
     */
    public function handling(Request $request): string
    {
        switch ($request->method) {
            case "POST":
                return $this->migrate();
            default:
                return $this->response(["message" => "Method Not Allowed"], self::HTTP_NOT_ALLOWED);
        }
    }

    /**
     * @return string|void
     */
    public function migrate()
    {
        $statement = <<<EOS
    CREATE TABLE IF NOT EXISTS users (
        id INT NOT NULL AUTO_INCREMENT,
        number BIGINT NOT NULL,
        name VARCHAR(50) NOT NULL,
        PRIMARY KEY (id)
    ) ENGINE=INNODB;

    CREATE TABLE IF NOT EXISTS news (
        id INT NOT NULL AUTO_INCREMENT,
        name VARCHAR(50) NOT NULL,
        content TEXT NOT NULL,
        PRIMARY KEY (id)
    ) ENGINE=INNODB;

    CREATE TABLE IF NOT EXISTS queue (
        id INT NOT NULL AUTO_INCREMENT,
        user_id INT NOT NULL,
        news_id INT NOT NULL,
        PRIMARY KEY (id),
        FOREIGN KEY (user_id) REFERENCES users(id),
        FOREIGN KEY (news_id) REFERENCES news(id)
    ) ENGINE=INNODB;

    CREATE TABLE IF NOT EXISTS user_news (
        user_id INT NOT NULL,
        news_id INT NOT NULL,
        PRIMARY KEY (user_id, news_id),
        FOREIGN KEY (user_id) REFERENCES users(id),
        FOREIGN KEY (news_id) REFERENCES news(id)
    ) ENGINE=INNODB;
EOS;

        try {
            (new DbConnector())->getConnection()->exec($statement);
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }

        return $this->response(["message" => "Success"], self::HTTP_OK);
    }
}