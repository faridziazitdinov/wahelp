<?php

namespace App\Controllers;

use App\Entity\Request;

abstract class Controller
{
    public const HTTP_OK = 200;
    public const HTTP_BAD_REQUEST = 400;
    public const HTTP_NOT_ALLOWED = 405;
    public const HTTP_SERVER_ERROR = 500;

    private array $statuses = [
        self::HTTP_OK => "Ok",
        self::HTTP_BAD_REQUEST => "Bad Request",
        self::HTTP_NOT_ALLOWED => "Method Not Allowed",
        self::HTTP_SERVER_ERROR => "Internal Server Error"
    ];

    public function __construct()
    {
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: text/csv; charset=utf-8");
    }

    /**
     * @param array $data
     * @param int $status
     * @return string
     */
    protected function response(array $data, int $status = self::HTTP_SERVER_ERROR): string
    {
        header("HTTP/1.1 {$status} {$this->statuses[$status]}");
        header("Content-Type: application/json");
        return json_encode($data);
    }

    /**
     * @param Request $request
     * @return string
     */
    abstract public function handling(Request $request): string;
}