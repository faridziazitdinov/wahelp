<?php

namespace App\Controllers;

use App\Entity\Request;
use App\Services\NewsletterService;

class Newsletter extends Controller
{
    public function __construct()
    {
        header("Access-Control-Allow-Methods: POST");
        parent::__construct();
    }

    /**
     * @param Request $request
     * @return string
     */
    public function handling(Request $request): string
    {
        switch ($request->method) {
            case "POST":
                if (isset($request->uri[3])) {
                    switch ($request->uri[3]) {
                        case "send":
                            return $this->sendNewsletter($request);
                        case "send-all":
                            return $this->sendNewsletterAllUsers($request);
                        case "start":
                            return $this->startQueueNewsletter();
                        default:
                            return $this->response(["message" => "Method Not Allowed"], self::HTTP_NOT_ALLOWED);
                    }
                }
            default:
                return $this->response(["message" => "Method Not Allowed"], self::HTTP_NOT_ALLOWED);
        }
    }

    /**
     * @param Request $request
     * @return string
     */
    public function sendNewsletter(Request $request): string
    {
        try {
            if (!isset($request->body['news']) || !$request->body['news'] || !is_int($request->body['news'])) {
                return $this->response(["message" => "Not Valid News"], self::HTTP_BAD_REQUEST);
            }
            (new NewsletterService())->sendNewsletter($request->body['news']);
        } catch (\Exception $e) {
            return $this->response(["message" => $e->getMessage()], self::HTTP_SERVER_ERROR);
        }

        return $this->response(["message" => "Success"], self::HTTP_OK);
    }

    /**
     * @param Request $request
     * @return string
     */
    public function sendNewsletterAllUsers(Request $request): string
    {
        try {
            if (!isset($request->body['news']) || !$request->body['news'] || !is_int($request->body['news'])) {
                return $this->response(["message" => "Not Valid News"], self::HTTP_BAD_REQUEST);
            }
            (new NewsletterService())->sendNewsletterAllUsers($request->body['news']);
        } catch (\Exception $e) {
            return $this->response(["message" => $e->getMessage()], self::HTTP_SERVER_ERROR);
        }

        return $this->response(["message" => "Success"], self::HTTP_OK);
    }


//cron
    /**
     * @return string
     */
    public function startQueueNewsletter(): string
    {
        (new NewsletterService())->asyncSendNewsletter();
        return $this->response(["message" => "Success"], self::HTTP_OK);
    }
}