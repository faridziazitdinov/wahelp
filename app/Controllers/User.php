<?php

namespace App\Controllers;

use App\Entity\Request;
use App\Services\UserService;

class User extends Controller
{
    private const FILE_TYPE = 'text/csv';

    public function __construct()
    {
        header("Access-Control-Allow-Methods: POST");
        parent::__construct();
    }

    /**
     * @param Request $request
     * @return string
     */
    public function handling(Request $request): string
    {
        switch ($request->method) {
            case "POST":
                return $this->saveUsers($request);
            default:
                return $this->response(["message" => "Method Not Allowed"], self::HTTP_NOT_ALLOWED);
        }
    }

    /**
     * @param Request $request
     * @return string
     */
    private function saveUsers(Request $request): string
    {
        if (!isset($request->files['users'])) {
            return $this->response(["message" => "Not File Or Incorrect Name"], self::HTTP_BAD_REQUEST);
        }

        $data = $request->files['users'];
        if ($data['size'] > 1) {
            if ($data['type'] != self::FILE_TYPE) {
                return $this->response(["message" => "Unsupported File Type"], self::HTTP_BAD_REQUEST);
            }

            try {
                $service = new UserService();
                $service->saveUsers($data['tmp_name']);
            } catch (\Exception $e) {
                return $this->response(["message" => $e->getMessage()], self::HTTP_SERVER_ERROR);
            }
        }

        return $this->response(["message" => "Success"], self::HTTP_OK);
    }
}