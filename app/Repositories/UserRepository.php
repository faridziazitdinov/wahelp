<?php

namespace App\Repositories;

class UserRepository
{
    private \PDO $conn;

    public function __construct(\PDO $conn)
    {
        $this->conn = $conn;
    }

    public function saveUsers(array $users)
    {
        $statement = "INSERT INTO users (number, name) VALUES (:number, :name);";
        $statement = $this->conn->prepare($statement);

        $this->conn->beginTransaction();
        foreach ($users as $user) {
            $statement->execute(["number" => $user[0], "name" => $user[1]]);
        }

        if (!$this->conn->commit()) {
            throw new \Exception("Failed To Save");
        }
    }

    /**
     * @param array $users
     * @return void
     * @throws \Exception
     */
    public function saveUsersV2(array $users)
    {
        $statement = "INSERT INTO users (number, name) VALUES " .
            str_repeat("(?,?),", count($users) - 1) . "(?,?)";
        $statement = $this->conn->prepare($statement);
        if (!$statement->execute(array_merge(...$users))) {
            throw new \Exception("Failed To Save");
        }
    }

    public function saveUsersV3(string $filePath)
    {
        $statement = "LOAD DATA INFILE '$filePath' INTO TABLE users FIELDS TERMINATED BY ',';";
        $statement = $this->conn->prepare($statement);
        if (!$statement->execute()) {
            throw new \Exception("Failed To Save");
        }
    }

    /**
     * @return array
     */
    public function getAllUsers(): array
    {
        $statement = "SELECT id FROM users";
        $statement = $this->conn->prepare($statement);
        $statement->execute();
        $ids = [];
        while ($userNews = $statement->fetch(\PDO::FETCH_ASSOC)) {
            $ids[] = $userNews;
        }
        return $ids;
    }
}