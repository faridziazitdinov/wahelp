<?php

namespace App\Repositories;

class QueueRepository
{
    private \PDO $conn;

    public function __construct($conn)
    {
        $this->conn = $conn;
    }

    /**
     * @param int $news
     * @return mixed
     */
    public function checkNews(int $news)
    {
        $statement = "SELECT EXISTS(SELECT * FROM news WHERE id = {$news} LIMIT 1)";
        return $this->conn->query($statement)->fetch();
    }

    /**
     * @param int $news
     * @return array
     */
    public function getUsersNewsIds(int $news): array
    {
        $statement = "SELECT * FROM user_news WHERE news_id = ?";
        $statement = $this->conn->prepare($statement);
        $statement->execute([$news]);
        $ids = [];
        while ($userNews = $statement->fetch(\PDO::FETCH_ASSOC)) {
            $ids[] = $userNews;
        }
        return $ids;
    }

    /**
     * @param array $ids
     * @return bool
     */
    public function save(array $ids): bool
    {
        $statement = "INSERT INTO queue (user_id, news_id) VALUES " .
            str_repeat("(?,?),", count($ids) - 1) . "(?,?)";
        $statement = $this->conn->prepare($statement);
        return $statement->execute(array_merge(...$ids));
    }

    /**
     * @param int $id
     * @return bool
     */
    public function delete(int $id): bool
    {
        $statement = $this->conn->prepare("DELETE FROM queue WHERE id = ?");
        return $statement->execute([$id]);
    }

    /**
     * @param int $limit
     * @return array|false
     */
    public function getMessages(int $limit)
    {
        $statement = "SELECT * FROM queue LIMIT {$limit};";
        $statement = $this->conn->query($statement);
        return $statement->fetchAll(\PDO::FETCH_ASSOC);
    }

    /**
     * @param int $user
     * @param int $news
     * @return array|false
     */
    public function getUserNews(int $user, int $news)
    {
        $statement = "SELECT users.name, news.name as news_name, news.content FROM queue 
    JOIN users ON queue.user_id = users.id 
    JOIN news ON queue.news_id = news.id WHERE users.id = ? AND news.id = ?";
        $statement = $this->conn->prepare($statement);
        $statement->execute([$user, $news]);
        return $statement->fetchAll(\PDO::FETCH_ASSOC);
    }
}