<?php

namespace App;

use App\Controllers\Migration;
use App\Controllers\User;
use App\Controllers\Newsletter;
use App\Entity\Request;

class App
{
    private const URI_API = "api";

    public function run()
    {
        $request = $this->request();

        if (!isset($request->uri[1]) || $request->uri[1] != self::URI_API || !isset($request->uri[2])) {
            $this->response();
            return;
        }

        switch ($request->uri[2]) {
            case "user":
                return (new User())->handling($request);
            case "newsletter":
                return (new Newsletter())->handling($request);
            case "migration":
                return (new Migration())->handling($request);
            default:
                $this->response();
        }
    }

    private function response(): void
    {
        header("HTTP/1.1 405 Method Not Allowed");
        header("Content-Type: application/json");
    }

    private function request()
    {
        $uri = explode("/", parse_url($_SERVER["REQUEST_URI"], PHP_URL_PATH));
        $data = json_decode(file_get_contents('php://input'), true);
        return new Request($_SERVER["REQUEST_METHOD"], $uri, $data ?? [], $_FILES);
    }
}