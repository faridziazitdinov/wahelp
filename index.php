<?php

require 'vendor/autoload.php';

use App\App;

try {
    echo (new App())->run();
} catch (Exception $e) {
    echo json_encode(['error' => $e->getMessage()]);
}